
import os
import glob
import trimesh
import numpy as np
import pyntcloud
import pandas as pd
from pyntcloud import PyntCloud
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

DATA_DIR = r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\train_data"
folder_names = os.listdir(DATA_DIR)
# Extract the Train Points
def get_train_point():
    train_points = np.array([])
    for i, folder_name in enumerate(folder_names):
        #print(i)
        #print(folder_name)
        folder = os.path.join(DATA_DIR, folder_name)
        #print(folder)
        train_files = glob.glob(os.path.join(folder, "*.pts"))
        #print(train_files)
        for f in train_files:
            # print("train files",f)
            data = PyntCloud.from_file(f, sep=" ",
                            header=0,
                            names=["x","y","z"])
            train_points=np.append(train_points, data.points )

    return train_points.reshape(16,-1,3)
train_points = get_train_point()
print(train_points)

# GET lABEL
DATA_DIR2 = r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\train_label"
folder_names3 = os.listdir(DATA_DIR2)
def get_train_label():
    label_points = np.array([])
    for i, folder_name in enumerate(folder_names2):
        folder = os.path.join(DATA_DIR2, folder_name)
        label_files = glob.glob(os.path.join(folder, "*.seg"))
        print(label_files)
        for f in label_files:
            label= pd.read_csv(f).to_numpy()
            label_points = np.append(label_points,label)
    return label_points.reshape(16,-1)
train_labels = get_train_label()
print(train_labels)


#Get test data
DATA_DIR3 = r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_data"
folder_names3 = os.listdir(DATA_DIR3)
# Extract the Train Points
def get_test_point():
    test_points = np.array([])
    for i, folder_name in enumerate(folder_names):
        #print(i)
        #print(folder_name)
        folder = os.path.join(DATA_DIR3, folder_name)
        #print(folder)
        test_files = glob.glob(os.path.join(folder, "*.pts"))
        #print(train_files)
        for f in test_files:
            # print("train files",f)
            data = PyntCloud.from_file(f, sep=" ",
                            header=0,
                            names=["x","y","z"])
            test_points=np.append(test_points, data.points )

    return test_points.reshape(16,-1,3)
test_points = get_test_point()

#get test label
DATA_DIR4 = r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_label"
folder_names4 = os.listdir(DATA_DIR4)
def get_test_label():
    label_test = np.array([])
    for i, folder_name in enumerate(folder_names4):
        folder = os.path.join(DATA_DIR2, folder_name)
        label_files = glob.glob(os.path.join(folder, "*.seg"))
        print(label_files)
        for f in label_files:
            label= pd.read_csv(f).to_numpy()
            label_test = np.append(label_test,label)
    return label_test.reshape(16,-1)
test_labels = get_test_label()



BATCH_SIZE = 32



def augment(points, label):
    # jitter points
    points += tf.random.uniform(points.shape, -0.005, 0.005, dtype=tf.float64)
    # shuffle points
    points = tf.random.shuffle(points)
    return points, label


train_dataset = tf.data.Dataset.from_tensor_slices((train_points,train_labels,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]))
test_dataset = tf.data.Dataset.from_tensor_slices((test_points,test_labels,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]))
# a = list(train_dataset.as_numpy_iterator())
# print(a)


