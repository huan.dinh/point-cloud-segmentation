import os
import sys
import glob
import numpy as np
import matplotlib.pyplot as plt
import pyntcloud
import tensorflow as tf
from pyntcloud import PyntCloud
import pandas as pd

DATA_DIR = "F:\GIT\Mesh_classi\dev3_data"

data = pd.read_csv(r'F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_label\02691156\012137.seg')
datal = pd.read_csv(r'F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_label\02691156\012141.seg')
print(data)
f =r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_data\02691156\012137.pts"
fl =r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_data\02691156\012141.pts"
data2 = PyntCloud.from_file(f, sep=" ",
                            header=0,
                            names=["x","y","z"])

data2t = PyntCloud.from_file(fl, sep=" ",
                            header=0,
                            names=["x","y","z"])
data_train = np.array([])
data_train = np.append(data_train,np.array(data2.points)).reshape(-1,3)
print(data_train)
data_train = np.append(data_train,np.array(data2t.points))
print(data_train.reshape(2,-1,3))
label_data = np.append(data,datal)
train_dataset = tf.data.Dataset.from_tensor_slices((data_train.reshape(2,-1,3),label_data.reshape(2,-1)))
train_dataset
# points = data2.points
#
# fig = plt.figure(figsize=(5, 5))
# ax = fig.add_subplot(111, projection="3d")
# ax.scatter(points[:, 0], points[:, 1], points[:, 2])
# ax.set_axis_off()
# plt.show()