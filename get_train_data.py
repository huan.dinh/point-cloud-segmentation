import os
import glob
import numpy as np
import pyntcloud
import pandas as pd
from pyntcloud import PyntCloud
import matplotlib.pyplot as plt
DATA_DIR = r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_data"
folder_names = os.listdir(DATA_DIR)
# get Train Points
def get_train_point():
    train_points = []
    part_labels = []
    for i, folder_name in enumerate(folder_names):
        #print(i)
        #print(folder_name)
        folder = os.path.join(DATA_DIR, folder_name)
        #print(folder)
        train_files = glob.glob(os.path.join(folder, "*.pts"))
        #print(train_files)
        for f in train_files:
            # print("train files",f)
            tran_array = np.array([])
            data = PyntCloud.from_file(f, sep=" ",
                            header=0,
                            names=["x","y","z"])
            tran_array=np.append(tran_array,np.array(data.points)).reshape(-1,3)
            train_points.append( tran_array[:1024])
            # train_points = np.append(train_points,tran_array[:1024])
            part_labels.append(i)

    return (np.array(train_points),
            np.array(part_labels))
train_points, train1_labels = get_train_point()
print(train_points)
print(train_points.shape)
print(train_points[1].shape)
print(train1_labels)

points = train_points[506]

fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111, projection="3d")
ax.scatter(points[:, 0], points[:, 1], points[:, 2])
ax.set_axis_off()
plt.show()


#Get label
# DATA_DIR2 = r"F:\GIT\mesh-seg\point-cloud-segmentation\dataset\val_label"
# folder_names2 = os.listdir(DATA_DIR2)
# def get_train_label():
#     label_points = []
#     for i, folder_name in enumerate(folder_names2):
#         folder = os.path.join(DATA_DIR2, folder_name)
#         label_files = glob.glob(os.path.join(folder, "*.seg"))
#         #print(label_files)
#         for f in label_files:
#             label= pd.read_csv(f).to_numpy()
#             label_points.append(label)
#     return np.array(label_points)
# train_labels = get_train_label()
# print(train_labels)
